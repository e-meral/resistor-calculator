# Resistor Calculator

## Description
This is a simple tool that calculates the value of a standard band-colored resistor that can have 4, 5 or 6 bands.

## Installation
This CMake project has been written in C++. Currently developed and tested on GNU/Linux.

Before trying to run the application, make sure the following has been already installed on your PC:

- g++ (version 9 or newer)
- cmake (version 3.16 or newer)

After cloning the repo, open the terminal on project root-level and execute the following:

Initialise submodules:
```
git submodule update --init --recursive
```

Make a new folder with name 'bin' and run CMake inside that folder:
```
cmake . -Bbin
```

Build application and unit-test:
```
make -C bin all
```

Run application:
```
./bin/resistor_calculator
```

Run Unit-Tests:
```
./bin/Tests/UnitTests/unit_tests
```

## Usage
Currently the tool supports only command-line interface. After starting the application it asks for total amount of bands which can be 4, 5 or 6. After entering a valid number, the colors should be entered.

After entering the color of the latest band, the tool will print the value and tolerance of the resistor.

## Note
Project "Resistor Calculator" is a educative hobby project and comes with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law.

## License