#include <iostream>
#include "version.h"
#include "menu.h"

void PrintWelcomeScreen(void)
{
	std::cout << "==============================================================" << std::endl;
	std::cout << "Resistor value calculator [v" APP_VERSION << "]" << std::endl << std::endl;
	std::cout << "        _____               _____" << std::endl;
	std::cout << "       /  || \\=-----------=/ ||  \\" << std::endl;
	std::cout << "      |   ||     ||   ||     ||   |" << std::endl;
	std::cout << "O=====|   ||     ||   ||     ||   |=====O" << std::endl;
	std::cout << "|     |   ||     ||   ||     ||   |     |" << std::endl;
	std::cout << "|      \\__||_/=-----------=\\_||__/      |" << std::endl;
	std::cout << "|                                       |" << std::endl;
	std::cout << "|                                       |" << std::endl << std::endl;
	std::cout << "==============================================================" << std::endl;
}