#include <iostream>
#include <stdexcept>
#include "resistor.h"


resistor_band_t resistor_lookup_table[12] = {
// | BAND COLOR TABLE IDX  | Band color | Mantisse value | Multiplication factor | Tolerance | Temperature coefficient |
// |                       |            |                |                       |    (%)    |         (ppm/K)         |
   { band_colors_t::SILVER , "silver"   , 0              , 0.01                  , 10.0          ,   0                 },
   { band_colors_t::GOLD   , "gold"     , 0              , 0.1                   ,  5.0          ,   0                 },
   { band_colors_t::BLACK  , "black"    , 0              , 1                     ,  0.0          , 250                 },
   { band_colors_t::BROWN  , "brown"    , 1              , 10                    ,  1.0          , 100                 },
   { band_colors_t::RED    , "red"      , 2              , 100                   ,  2.0          ,  50                 },
   { band_colors_t::ORANGE , "orange"   , 3              , 1000                  ,  0.0          ,  15                 },
   { band_colors_t::YELLOW , "yellow"   , 4              , 10000                 ,  0.0          ,  25                 },
   { band_colors_t::GREEN  , "green"    , 5              , 100000                ,  0.5          ,  20                 },
   { band_colors_t::BLUE   , "blue"     , 6              , 1000000               ,  0.25         ,  10                 },
   { band_colors_t::VIOLET , "violet"   , 7              , 10000000              ,  0.1          ,   5                 },
   { band_colors_t::GREY   , "grey"     , 8              , 100000000             ,  0.05         ,   1                 },
   { band_colors_t::WHITE  , "white"    , 9              , 1000000000            ,  0.0          ,   0                 }
};

template<typename T, int N>
constexpr int GetArrayCount(T(&)[N]) {
  return N;
}



// Constructor
Resistor::Resistor(int numberOfBands) {
  if ((numberOfBands < MINIMUM_NUMBER_OF_BANDS) || (numberOfBands > MAXIMUM_NUMBER_OF_BANDS)) {
    throw std::invalid_argument("Number of bands can only be 4, 5 or 6. Entered: " +
      std::to_string(numberOfBands) +
      "\n\nProgram has been terminated!\n");
  }  
  this->m_numberOfBands = numberOfBands;
  this->m_band_color_table_index.resize(numberOfBands);
}


// Destructor
Resistor::~Resistor() {}


bool Resistor::setBandColor(int bandNumber, std::string bandColor) {
  if (bandNumber > this->getNumberOfBands()) {
    return false;
  }

  for (int idx=0; idx<GetArrayCount(resistor_lookup_table); idx++) {
    if (bandColor.compare(resistor_lookup_table[idx].band_color) == 0) {
      this->m_band_color_table_index[bandNumber] = resistor_lookup_table[idx].band_color_table_index;
      return true;
    }
  }
  return false;
}


std::string Resistor::getBandColor(int bandNumber) const {
  auto a = this->m_band_color_table_index[bandNumber];
  return resistor_lookup_table[(int)a].band_color;
}


int Resistor::getNumberOfBands() const {
  return m_numberOfBands;
}


void Resistor::calculateValue() {
  if(this->getNumberOfBands() == 4) {
    this->m_resistor_value = (( (10 * resistor_lookup_table[(int)this->m_band_color_table_index[0]].mantisse_value) +
                                      resistor_lookup_table[(int)this->m_band_color_table_index[1]].mantisse_value) *
                                     resistor_lookup_table[(int)this->m_band_color_table_index[2]].multiplication_factor);
  }
  else if ( (this->getNumberOfBands() == 5) || (this->getNumberOfBands() == 6) ) {
    this->m_resistor_value = (( (100 * resistor_lookup_table[(int)this->m_band_color_table_index[0]].mantisse_value) +
                                ( 10 * resistor_lookup_table[(int)this->m_band_color_table_index[1]].mantisse_value) +
                                       resistor_lookup_table[(int)this->m_band_color_table_index[2]].mantisse_value) *
                                       resistor_lookup_table[(int)this->m_band_color_table_index[3]].multiplication_factor);
  }
}

void Resistor::calculateTolerance() {
  if(this->getNumberOfBands() == 4) {
    this->m_resistor_tolerance = resistor_lookup_table[(int)this->m_band_color_table_index[3]].tolerance;
  }
  else if ( (this->getNumberOfBands() == 5) || (this->getNumberOfBands() == 6) ) {
    this->m_resistor_tolerance = resistor_lookup_table[(int)this->m_band_color_table_index[4]].tolerance;  
  }
}

void Resistor::calculateTemperatureCoefficient() {
    if(this->getNumberOfBands() == 6) {
    this->m_resistor_temperature_coefficient = resistor_lookup_table[(int)this->m_band_color_table_index[5]].temperature_coefficient;
  }
}

float Resistor::getResistorValue() {
  this->calculateValue();
  return this->m_resistor_value;
}

float Resistor::getResistorTolerance() {
  this->calculateTolerance();
  return this->m_resistor_tolerance;
}

int Resistor::getResistorTemperatureCoefficient() {
  this->calculateTemperatureCoefficient();
  return this->m_resistor_temperature_coefficient;
}