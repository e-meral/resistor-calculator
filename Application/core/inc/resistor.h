#pragma once

#include <array>
#include <vector>
#include <string>

#define MINIMUM_NUMBER_OF_BANDS 4
#define MAXIMUM_NUMBER_OF_BANDS 6


enum class band_colors_t {
  SILVER  =  0,
  GOLD    =  1,
  BLACK   =  2,
  BROWN   =  3,
  RED     =  4,
  ORANGE  =  5,
  YELLOW  =  6,
  GREEN   =  7,
  BLUE    =  8,
  VIOLET  =  9,
  GREY    = 10,
  WHITE   = 11
};

struct resistor_band_t {
  band_colors_t band_color_table_index;
  std::string band_color;
  int mantisse_value;
  double multiplication_factor;
  float tolerance;
  int temperature_coefficient;
};

extern resistor_band_t resistor_lookup_table[];


class Resistor {
  private:
    int m_numberOfBands;
    std::vector<band_colors_t> m_band_color_table_index;
    float m_resistor_value;
    float m_resistor_tolerance;
    int m_resistor_temperature_coefficient;

    void calculateValue();
    void calculateTolerance();
    void calculateTemperatureCoefficient();

  public:
    // Constuctor
    explicit Resistor(int numberOfBands);

    // Destructor
    ~Resistor();

    bool setBandColor(int bandNumber, std::string bandColor);
    std::string getBandColor(int bandNumber) const;
    int getNumberOfBands() const;

    float getResistorValue();
    float getResistorTolerance();
    int getResistorTemperatureCoefficient();
};
