#include <iostream>
#include "menu.h"
#include "version.h"
#include "resistor.h"

void PrintWelcomeScreen(void);

int main(void)
{
	int numberOfBands{0};
	std::string band_color;
	bool isEnteredColorValid{false};
	
	PrintWelcomeScreen();

	std::cout << "Please enter the number of bands (4, 5 or 6):" << std::endl;
	std::cin >> numberOfBands;

	Resistor r1(numberOfBands);

	for (auto bandnr=0; bandnr<r1.getNumberOfBands(); bandnr++) {
		std::cout << "Enter color of band number " << bandnr+1 << ":" << std::endl;
		std::cout << "Color options: " << "\'black\'"  << ", " <<
										  "\'brown\'"  << ", " <<
										  "\'red\'"    << ", " <<
										  "\'orange\'" << ", " <<
										  "\'yellow\'" << ", " <<
										  "\'green\'"  << ", " <<
										  "\'blue\'"   << ", " <<
										  "\'violet\'" << ", " <<
										  "\'grey\'"   << ", " <<
										  "\'white\'"  << std::endl;

		do {
			std::cin >> band_color;
			if(r1.setBandColor(bandnr, band_color) == false) {
				isEnteredColorValid = false;
				std::cout << "Wrong color has been entered, try again:" << std::endl;
			}
			else {
				isEnteredColorValid = true;
			}
		}while(isEnteredColorValid == false);
	}

	std::cout << std::endl << std::endl;
	std::cout << "=================" << std::endl;	
	std::cout << "Result:" << std::endl;
	std::cout << "=================" << std::endl;
	std::cout << "Number of bands: " << r1.getNumberOfBands() << std::endl;

	for (auto bandnr=0; bandnr<r1.getNumberOfBands(); bandnr++) {
		std::cout << "Band " << bandnr+1 << " color: " << r1.getBandColor(bandnr) << std::endl;
	}

	std::cout << std::endl;
	std::cout << "Resistor value: " << r1.getResistorValue() << " Ohm." << std::endl;
	std::cout << "Tolerance: " << r1.getResistorTolerance() << " %" << std::endl;
	if(r1.getNumberOfBands() == 6) {
		std::cout << "Temperature coefficient: " << r1.getResistorTemperatureCoefficient() << " ppm/K" << std::endl;
	}
	std::cout << std::endl;

	return 0;
}

