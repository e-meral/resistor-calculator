#include "gtest/gtest.h"
#include "resistor.h"
#include <stdexcept>


TEST(RESISTOR, ConstructorException) {
    EXPECT_THROW(Resistor{1}, std::invalid_argument);
    EXPECT_THROW(Resistor{10}, std::invalid_argument);
}

TEST(RESISTOR, SetBandColorHappyFlow) {
    Resistor resistor{5};

    EXPECT_TRUE(resistor.setBandColor(1, "red"));
    EXPECT_TRUE(resistor.setBandColor(2, "brown"));
    EXPECT_TRUE(resistor.setBandColor(3, "yellow"));
    EXPECT_TRUE(resistor.setBandColor(4, "silver"));
    EXPECT_TRUE(resistor.setBandColor(5, "gold"));
}

TEST(RESISTOR, SetBandColorError) {
    Resistor resistor{5};

    EXPECT_FALSE(resistor.setBandColor(6, "red"));    
    EXPECT_FALSE(resistor.setBandColor(12, "Black"));
    EXPECT_FALSE(resistor.setBandColor(2, "magenta"));
}

TEST(RESISTOR, GetBandColorHappyFlow) {
    Resistor resistor{5};

    EXPECT_TRUE(resistor.setBandColor(1, "brown"));
    EXPECT_EQ(resistor.getBandColor(1), "brown");

    EXPECT_TRUE(resistor.setBandColor(2, "black"));
    EXPECT_EQ(resistor.getBandColor(2), "black");    
}

TEST(RESISTOR, GetBandColorError) {
    Resistor resistor{5};

    EXPECT_TRUE(resistor.setBandColor(2, "black"));
    EXPECT_NE(resistor.getBandColor(2), "red");
}